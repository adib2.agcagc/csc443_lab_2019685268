<?php
//Question A
        $monthDays = array (
            'January' => 31,
            'Febuary' => 28,
            'March' => 31,
            'April' => 30,
            'May' => 31,
            'June' => 30,
            'July' => 31,
            'August' => 31,
            'September' => 30,
            'October' => 31,
            'November' => 30,
            'December' => 31
        );

     //Question B (foreach loop) 
     foreach($monthDays as $month => $days) {
        echo "Month= " . $month . ", Days in Month= " . $days;
        echo "<br>"; 
    }    

    //Question C
    echo "</br> <b>This is months that have 30 days: </b> </br>";
    foreach($monthDays as $month => $days) {
       if($days==30){
       echo "Month= " . $month;
       echo "<br>"; };
   }